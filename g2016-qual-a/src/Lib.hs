module Lib
    ( someFunc
  	, timeToSleep
    ) where

import qualified Data.Set as S
import Data.Text hiding (head, tail, map)
import Data.Text.IO
import Data.Text.Read
import System.Environment

someFunc :: String
someFunc = "hi"

timeToSleep :: Int -> Text
timeToSleep 0 = pack "INSOMNIA"
timeToSleep n = pack $ show $ (n * (timeToDigits 0 (S.fromList [0,1..9]) 
					 $ map listDigits $ map (n*) [1..]))

{-
	Int is the current count
	Set Int is the remaining unfound digits
	[Set Int] is the list of sets of digits of n, 2*n, ...
-}
timeToDigits :: Int -> S.Set Int -> [S.Set Int] -> Int
timeToDigits time digits sheep
	| S.null digits = time
	| otherwise   = timeToDigits (time+1) (S.difference digits $ head sheep) (tail sheep)


--take a int
--return a list of the digits in that int
listDigits :: Int -> S.Set Int
listDigits 0 = S.empty
listDigits x = S.insert (mod x 10) (listDigits $ div x 10)