module Main where

import Lib
import qualified Data.Set as S
import Data.Text hiding (head, tail, map, length)
import Data.Text.IO
import Data.Text.Read
import System.Environment

main :: IO ()
main = 
	getArgs >>= \arg -> do 
	inputCases <- getInputAsLines (head arg)
	let outputCases = map timeToSleep (map readInt (tail inputCases)) 
	let outputText = Data.Text.unlines $ zipText (makePrependText [1..(length inputCases)]) outputCases
	Data.Text.IO.writeFile outputPath outputText 
	
outputPath :: FilePath
outputPath = "out.txt"
--	let outputCases = map timeToSleep (map readInt (tail inputCases)) 
--	writeTextList "./output.txt" outputCases

{-
writeTextList :: FilePath -> [Text] -> [IO ()]
writeTextList path textList = do
	Data.Text.IO.writeFile path (head textList)
	map (Data.Text.IO.appendFile path) (tail textList) 
-}

{-
converts a list [1..n] into a list of text
[Case #N:..]
-}

makePrependText :: [Int] -> [Text]
makePrependText intList = map (Data.Text.append (pack "Case #")) 
							$ map pack
							$ map (++": ")
							$ map show intList

getValueFromText :: Reader a -> Text -> a
getValueFromText = (value .)
	where
		value (Right (v, _)) = v

readInt :: Text -> Int
readInt = getValueFromText decimal

getInputAsLines :: FilePath -> IO [Text]
getInputAsLines path = do
	 textInput <- Data.Text.IO.readFile path
	 return (Data.Text.lines textInput)

zipText :: [Text] -> [Text] -> [Text]
zipText _ [] = []
zipText [] _ = []
zipText (a:as) (b:bs) = (Data.Text.append a b) : zipText as bs