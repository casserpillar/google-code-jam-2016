module Lib
    ( someFunc
    , getNumFlips
    , toStack
    , Pancake
    , FlipType
    ) where

data Pancake = Plus | Minus deriving (Show, Eq)
data FlipType = FirstPlus | FirstMinus deriving (Show, Eq) 

someFunc :: IO ()
someFunc = putStrLn "someFunc"
{-
the loop

first, remove all happy pancakes from the bottom of the stack
	ignoreBottomHappyPancakes

then, if the stack is empty, return count of flips
	numPlus stack == length stack

then, if the beginning of the stack is +, flip all leading + and increment flips

then, flip the entire stack and increment flips
-}

toStack :: String -> [Pancake]
toStack "" = []
toStack stringStack = (toPancake $ head stringStack) : (toStack $ tail stringStack)

toPancake :: Char -> Pancake
toPancake a
	| a == '+' = Plus
	| a == '-' = Minus

getNumFlips :: Int -> [Pancake] -> Int
getNumFlips num stack 
	| isEmpty simpleStack	= num
	| otherwise 			= getNumFlips (num+1) (flipStack simpleStack)
	where simpleStack = ignoreBottomHappyPancakes stack

nextFlip :: [Pancake] -> FlipType
nextFlip a
	| isPlus $ head a 	= FirstPlus
	| otherwise 		= FirstMinus

flipStack :: [Pancake] -> [Pancake]
flipStack stack 
	| nextFlip stack == FirstPlus = (map flipCake $ reverse $ takeWhile isPlus stack) 
									++ dropWhile isPlus stack  
	| otherwise 				  = reverse $ map flipCake stack

flipCake :: Pancake -> Pancake
flipCake cake 
	| isPlus cake  = Minus
	| isMinus cake = Plus

ignoreBottomHappyPancakes :: [Pancake] -> [Pancake]
ignoreBottomHappyPancakes stack = reverse $ dropWhile isPlus $ reverse stack

numPlus :: [Pancake] -> Int
numPlus stack = length $ takeWhile isPlus stack

numMinus :: [Pancake] -> Int
numMinus stack = length $ takeWhile isMinus stack 

isPlus :: Pancake -> Bool
isPlus a
	| a == Plus  = True
	| otherwise  = False

isMinus :: Pancake -> Bool
isMinus a 
	| a == Minus  = True
	| otherwise   = False

isEmpty :: [Pancake] -> Bool
isEmpty a
	| a == []  = True
	| otherwise = False
