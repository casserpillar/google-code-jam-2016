module Main where

import Lib
import System.Environment
import Data.Text hiding (head, tail, map, length)
import Data.Text.IO
import Data.Text.Read

main :: IO ()
main = 
	getArgs >>= \arg -> do 
	inputCases <- getInputAsLines (head arg)
	let outputCases = map pack $ map show $ map (getNumFlips 0) $ map toStack $ map unpack $ tail inputCases

	let outputText = Data.Text.unlines $ zipText (makePrependText [1..(length inputCases)]) outputCases
	Data.Text.IO.writeFile outputPath outputText 

outputPath :: FilePath
outputPath = "out.txt"	

makePrependText :: [Int] -> [Text]
makePrependText intList = map (Data.Text.append (pack "Case #")) 
							$ map pack
							$ map (++": ")
							$ map show intList

getValueFromText :: Reader a -> Text -> a
getValueFromText = (value .)
	where
		value (Right (v, _)) = v

readInt :: Text -> Int
readInt = getValueFromText decimal

getInputAsLines :: FilePath -> IO [Text]
getInputAsLines path = do
	 textInput <- Data.Text.IO.readFile path
	 return (Data.Text.lines textInput)

zipText :: [Text] -> [Text] -> [Text]
zipText _ [] = []
zipText [] _ = []
zipText (a:as) (b:bs) = (Data.Text.append a b) : zipText as bs